package com.devcamp.relationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.relationshipordercustomer.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Long>{
    
}
