package com.devcamp.relationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.relationshipordercustomer.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long>{
    Customer findByid(long id);
}
